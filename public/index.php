<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
require '../vendor/autoload.php';

//INIT

$app = new \Slim\App;
$container = $app->getContainer();

$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('html', [
        'cache' => false
    ]);
    $view->addExtension(new \Slim\Views\TwigExtension(
        $container['router'],
        $container['request']->getUri()
    ));
    return $view;
};

//ROUTES

$app->get('/', function ($request, $response, $args) {
  return $this->view->render($response, 'home.html', homeCtrl($args));
});

//CONTROLLERS

function homeCtrl($args)
{
  $data = ['name' => 'max'];
  return $data;
}

//RUN

$app->run();
?>
